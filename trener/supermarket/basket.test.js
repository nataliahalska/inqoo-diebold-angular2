
(function () {
  console.log('testAddFirstProduct')
  setupTest()

  cart.addProduct('123')

  console.assert(cart.getItems()[0].product_id === '123', 'Bad id')
  console.assert(cart.getItems()[0].amount === 1, 'Bad amount')
  console.assert(cart.getItems()[0].subtotal === 100, 'Bad subtotal')
})();

(function () {
  console.log('testAddProductTwice')
  setupTest()

  cart.addProduct('123')
  cart.addProduct('123')

  console.assert(cart.getItems()[0].product_id === '123', 'Bad id')
  console.assert(cart.getItems()[0].amount === 2, 'Bad amount')
  console.assert(cart.getItems()[0].subtotal === 200, 'Bad subtotal')
})();

(function () {
  console.log('testAdd2Products')
  setupTest()

  cart.addProduct('123')
  cart.addProduct('234')

  console.assert(cart.getItems()[1].product_id === '234', 'Bad id')
  console.assert(cart.getItems()[1].amount === 1, 'Bad amount')
  console.assert(cart.getItems()[1].subtotal === 200, 'Bad subtotal')
})();


(function () {
  console.log('testAddProductTwice')
  setupTest()

  cart.addProduct('123')
  cart.addProduct('123')
  cart.addProduct('234')
  cart.removeProduct('123')
  cart.removeProduct('234')

  console.assert(cart.getItems()[0].product_id === '123', 'Bad id')
  console.assert(cart.getItems()[0].amount === 1, 'Bad amount')
  console.assert(cart.getItems()[0].subtotal === 100, 'Bad subtotal')
  console.assert(cart.getItems().length === 1, 'Item not removed')
})();


(function () {
  console.log('testTotals')
  setupTest()
  console.assert(cart.getTotal() == 0, 'Bad total')

  cart.addProduct('123')
  console.assert(cart.getTotal() == 100, 'Bad total')
  cart.addProduct('123')
  console.assert(cart.getTotal() == 200, 'Bad total')
  cart.addProduct('234')
  console.assert(cart.getTotal() == 400, 'Bad total')
  cart.removeProduct('123')
  console.assert(cart.getTotal() == 300, 'Bad total')
  cart.removeProduct('234')
  console.assert(cart.getTotal() == 100, 'Bad total')

})();

console.log('End of tests!')

/* === UTILS === */

function equals(x, y) {
  return JSON.stringify(x) === JSON.stringify(y)
}

function setupTest() {
  cart = new Cart()
  cart.products = new Products([
    { id: '123', price: 100 },
    { id: '234', price: 200 },
  ])
  console.assert(equals(cart.getItems(), []), 'Cart is not empty')
}