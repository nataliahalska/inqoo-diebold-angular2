https://ramdajs.com/
http://underscorejs.org/#where
https://randycoulman.com/blog/2016/05/31/thinking-in-ramda-combining-functions/



```js

canDrink = person => person.age >= 18
canDrive = person => Boolean(person.driverLicense)

either = function(fn1,fn2){
    return (x) => {
        return fn1(x) || fn2(x)
    }
}

both = (fn1,fn2) => x => fn1(x) && fn2(x)
get = prop => obj => obj[prop]

// canDrinkOrDrive = either(canDrink, canDrive)
canDrinkAndDrive = both(canDrink, canDrive);

// canDrinkAndDrive( { age: 18, driverLicense: true })
[ { age: 18, driverLicense: true, name:'Alice' } ]
.filter(canDrinkAndDrive)
.map(get('name'))

```

```js

pipe(
  multiply(2),
  add(1),
  square(2),
  pow
)

// pow = (x,y) => Math.pow(x,y)

pow = x => y => Math.pow(x,y)
multiply = x => y => x * y;

// x => square(addOne(multiply(x)))


```


```js

const publishedInYear1 = (book, year) =>  book.year === year
const publishedInYear2 = year => book => book.year === year
const get = prop => obj => obj[prop]
// const getTitle = book => book.title
const getBookTitle = get('title')

const titlesForYear = (books, year) => {
//   const selected = filter(book => publishedInYear1(book,year), books)
  const selected = filter(publishedInYear2(year), books)
 
  return map(getBookTitle, selected)
}
```